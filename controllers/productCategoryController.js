const ProductCategory = require('../models/ProductCategory');

const getProductCategory = async (req, res) => {
   try {
      const productCategory = await ProductCategory.find();
      res.status(200).json({
         status: true,
         productCategory: productCategory.map((product_category) => ({
            id: product_category._id,
            name_category: product_category.name_category,
         })),
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const storeProductCategory = async (req, res) => {
   try {
      const { name_category } = req.body;
      if(!name_category) { throw { code: 428, message: 'name_category is required'} }

      const data = new ProductCategory({
         name_category: name_category
      });
      const result = await data.save();

      res.status(200).json({
         status: true,
         data: result
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const getProductCategoryById = async (req, res) => {
   try {
      const productCategoryId = req.params.id;
      const productCategory = await ProductCategory.findOne({ _id:productCategoryId});
      if(!productCategory) { throw { code: 404, message: 'data not found'}};
      res.status(200).json({
         status: true,
         productCategory
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const updateProductCategory = async (req, res) => {
   try {
      const productCategoryId = req.params.id;
      const data = {
         name_category: req.body.name_category
      };
      const productCategory = await ProductCategory.findOneAndUpdate({_id:productCategoryId}, data);
      if(!productCategory) { throw { code:404, message: 'data not found'}}
      res.status(200).json({status: true, message:'Updated data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const deleteProductCategory = async (req, res) => {
   try {
      const productCategoryId = req.params.id;
      const productCategory = await ProductCategory.findOneAndDelete({_id:productCategoryId});
      if(!productCategory) { throw { code:404, message: 'data not found'}}
      res.status(200).json({status: true, message:'Delete data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

module.exports = {
   getProductCategory,
   storeProductCategory,
   getProductCategoryById,
   updateProductCategory,
   deleteProductCategory
}