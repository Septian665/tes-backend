const product = require('../models/Proudct');
const variant = require('../models/Variant');
const productCategory = require('../models/ProductCategory');
const productImage = require('../models/ProductImage');
const mongoose = require('mongoose');

const getProduct = async (req, res) => {
   try {
      const dataProduct = await product.find();
      res.status(200).json({
         status: true,
         // data: dataProduct
         data: dataProduct.map((products) => ({
            id: products._id,
            name_product: products.name_product,
            price: products.price,
            product_imageId: products.product_imageId,
         })),
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const storeProduct = async (req, res) => {
   try {
      const { name_product, description, price, name_variantId, product_categoryId, product_imageId } = req.body;
      if(!name_product) { throw { code: 428, message: 'name_category is required'} }
      if(!description) { throw { code: 428, message: 'description is required'} }
      if(!price) { throw { code: 428, message: 'price is required'} }

      if(!mongoose.Types.ObjectId.isValid(name_variantId)) {
         { throw { code: 500 ,message: 'name_variantId invalid' }}
      }
      if(!mongoose.Types.ObjectId.isValid(product_categoryId)) {
         { throw { code: 500 ,message: 'product_categoryId invalid' }}
      }
      if(!mongoose.Types.ObjectId.isValid(product_imageId)) {
         { throw { code: 500 ,message: 'product_imageId invalid' }}
      }

      const variantExist = await variant.findOne({_id: name_variantId})
      if(!variantExist) { throw { code: 428,message: 'name_variantId is not exist' }}

      const productCategoryExist = await productCategory.findOne({_id: product_categoryId})
      if(!productCategoryExist) { throw { code: 428,message: 'product_categoryId is not exist' }}

      const productImageExist = await productImage.findOne({_id: product_imageId})
      if(!productImageExist) { throw { code: 428,message: 'product_imageId is not exist' }}

      const data = new product({
         name_product: name_product,
         description: description,
         price: price,
         name_variantId: name_variantId,
         product_categoryId: product_categoryId,
         product_imageId: product_imageId,
      });
      const result = await data.save();

      if(!result) {
         throw {
            code: 500,
            message: "store product failed"
         }
      }

      res.status(200).json({
         status: true,
         data: result
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const getProductById = async (req, res) => {
   try {
      const productId = req.params.id;
      const dataProduct = await product.findOne({ _id:productId});
      if(!dataProduct) { throw { code: 404, message: 'data not found'}};
      res.status(200).json({
         status: true,
         dataProduct
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const updateProduct = async (req, res) => {
   try {
      const productId = req.params.id;
      const { name_product, description, price, name_variantId, product_categoryId, product_imageId } = req.body
      const data = {
         name_product: name_product,
         description: description,
         price: price,
         name_variantId: name_variantId,
         product_categoryId: product_categoryId,
         product_imageId: product_imageId,
      };
      const dataProduct = await product.findOneAndUpdate({_id:productId}, data);
      if(!dataProduct) { throw { code:404, message: 'data not found'}}

      const variantExist = await variant.findOne({_id: name_variantId})
      if(!variantExist) { throw { code: 428,message: 'name_variantId is not exist' }}

      const productCategoryExist = await productCategory.findOne({_id: product_categoryId})
      if(!productCategoryExist) { throw { code: 428,message: 'product_categoryId is not exist' }}

      const productImageExist = await productImage.findOne({_id: product_imageId})
      if(!productImageExist) { throw { code: 428,message: 'product_imageId is not exist' }}
      
      res.status(200).json({status: true, message:'Updated data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const deleteProduct = async (req, res) => {
   try {
      const productId = req.params.id;
      const dataProduct = await product.findOneAndDelete({_id:productId});
      if(!dataProduct) { throw { code:404, message: 'data not found'}}
      res.status(200).json({status: true, message:'Delete data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

module.exports = {
   getProduct,
   storeProduct,
   getProductById,
   updateProduct,
   deleteProduct
}