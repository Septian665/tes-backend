const ProductImages = require('../models/ProductImage');
const fs = require('fs')

const getProductImage = async (req, res) => {
   try {
      const productImages = await ProductImages.find();
      res.status(200).json({
         status: true,
         
         ProductImages: productImages.map((product_image) => ({
            id: product_image._id,
            image: product_image.image,
         })),
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const storeProductImage = async (req, res) => {
   try {
      if(!req.file) { throw { code: 428, message: 'Please upload a valid image'} }

      const data = new ProductImages({
         image: req.file.path
      });
      const result = await data.save();

      res.status(200).json({
         status: true,
         data: result
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const getProductImageById = async (req, res) => {
   try {
      const productImageId = req.params.id;
      const productImage = await ProductImages.findOne({ _id:productImageId});
      if(!productImage) { throw { code: 404, message: 'Image not found'}};
      res.status(200).json({
         status: true,
         productImage
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const updateProductImage = async (req, res) => {
   try {
      const productImageId = req.params.id;
      if(!req.file) { throw { code:428, message: 'Please upload your image'}}
      const data = {
         image: req.file.path
      };
      const productImage = await ProductImages.findOneAndUpdate({_id:productImageId}, data);
      if(!productImage) { throw { code:404, message: 'data not found'}}
      fs.unlinkSync(productImage.image);
      res.status(200).json({status: true, message:'Updated data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const deleteProductImage = async (req, res) => {
   try {
      const productImageId = req.params.id;
      const productImage = await ProductImages.findOneAndDelete({_id:productImageId});
      if(!productImage) { throw { code:404, message: 'data not found'}}
      fs.unlinkSync(productImage.image);
      res.status(200).json({status: true, message:'Delete data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

module.exports = {
   getProductImage,
   storeProductImage,
   getProductImageById,
   updateProductImage,
   deleteProductImage
}