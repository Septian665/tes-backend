const Variant = require('../models/Variant');

const getDataVariant = async (req, res) => {
   try {
      const variants = await Variant.find();
      res.status(200).json({
         status: true,
         variants: variants.map((variant) => ({
            id: variant._id,
            size: variant.size,
            color: variant.color
         })),
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const storeVariant = async (req, res) => {
   try {
      const { size, color} = req.body;
      if(!size) { throw { code: 428, message: 'size is required'} }
      if(!color) { throw { code: 428, message: 'color is required'} }

      const data = new Variant({
         size: size,
         color: color
      });

      const result = await data.save();

      res.status(200).json({
         status: true,
         data: result
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const getVariantById = async (req, res) => {
   try {
      const variantId = req.params.id;
      const variant = await Variant.findOne({ _id:variantId});
      if(!variant) { throw { code: 404, message: 'data not found'}};
      res.status(200).json({
         status: true,
         variant
      });
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const updateVariant = async (req, res) => {
   try {
      const variantId = req.params.id;
      const data = {
         size: req.body.size,
         color: req.body.color
      };
      const variant = await Variant.findOneAndUpdate({_id:variantId}, data);
      if(!variant) { throw { code:404, message: 'data not found'}}
      res.status(200).json({status: true, message:'Updated data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

const deleteVariant = async (req, res) => {
   try {
      const variantId = req.params.id;
      const variant = await Variant.findOneAndDelete({_id:variantId});
      if(!variant) { throw { code:404, message: 'data not found'}}
      res.status(200).json({status: true, message:'Delete data success'})
   } catch (error) {
      if(!error.code) { error.code = 500 }
      res.status(error.code).json({
         status: false,
         error: error.message
      });
   }
}

module.exports = { 
   getDataVariant, 
   storeVariant, 
   getVariantById, 
   updateVariant, 
   deleteVariant 
}