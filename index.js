const express = require('express');
const db = require('./connection');
// const multer = require('multer');
const variants = require('./routes/variants');
const productCategories = require('./routes/productCategories');
const productImages = require('./routes/productImages');
const products = require('./routes/products');

const app = express();
const PORT = 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false}));

app.use('/api/v1/variant',variants);
app.use('/api/v1/product_category',productCategories);
app.use('/api/v1/productImages',productImages);
app.use('/api/v1/products',products);


const connection = async () => {
   try {
      await db();
      app.listen(PORT, () => {
         console.log(`server running http://127.0.0.1:${PORT}`);
      })
   } catch (error) {
      console.log(error);
   }
}

connection();