const multer = require('multer');
const fileStorage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, './images');
   },
   filename: (req, file, cb) => {
      cb(null, file.originalname);
   }
})
const fileFilter = (req, file, cb) => {
   const allowedTypeImage = ['image/png','image/jpeg','image/jpg']
   if(allowedTypeImage.includes(file.mimetype)){
      cb(null, true);
   } else {
      cb(null, false);
   }
}
const upload = multer({storage: fileStorage, fileFilter: fileFilter});

module.exports = upload;