const mongoose = require('mongoose');

const ProductCategories = new mongoose.Schema({
   name_category: {
      type: String
   }
}, {
   timestamps: { currentTime: () => Math.floor(Date.now() / 1000)} 
});

module.exports = mongoose.model('Product_Categories', ProductCategories);