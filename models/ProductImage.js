const mongoose = require('mongoose');

const ProductImages = new mongoose.Schema({
   image: {
      type: String
   }
}, {
   timestamps: { currentTime: () => Math.floor(Date.now() / 1000)} 
});

module.exports = mongoose.model('Product_image', ProductImages);