const mongoose = require('mongoose');

const Product = new mongoose.Schema({
   name_product: {
      type: String
   },
   description: {
      type: String
   },
   price: {
      type: Number
   },
   name_variantId: {
      type: mongoose.Schema.Types.ObjectId
   },
   product_categoryId: {
      type: mongoose.Schema.Types.ObjectId
   },
   product_imageId: {
      type: mongoose.Schema.Types.ObjectId
   }
}, {
   timestamps: { currentTime: () => Math.floor(Date.now() / 1000)} 
});

Product.virtual('variants', {
   ref: 'variants',
   localField: 'name_variantId',
   foreignField: '_id'
});

Product.virtual('product_categories', {
   ref: 'product_categories',
   localField: 'product_categoryId',
   foreignField: '_id'
});

Product.virtual('variants', {
   ref: 'product_image',
   localField: 'product_imageId',
   foreignField: '_id'
});

module.exports = mongoose.model('Product', Product);