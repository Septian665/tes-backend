const mongoose = require('mongoose');

const VariantSchema = new mongoose.Schema({
   size: {
      type: String
   },
   color: {
      type: String
   }
}, {
   timestamps: { currentTime: () => Math.floor(Date.now() / 1000)} 
});

module.exports = mongoose.model('Variants', VariantSchema);