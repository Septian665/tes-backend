const express = require('express');
const router = express.Router();
const {
   getProductCategory,
   storeProductCategory,
   getProductCategoryById,
   updateProductCategory,
   deleteProductCategory
} = require('../controllers/productCategoryController');

router.route('/').get(getProductCategory).post(storeProductCategory);
router.route('/:id').get(getProductCategoryById).put(updateProductCategory).delete(deleteProductCategory);
module.exports = router;