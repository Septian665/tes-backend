const express = require('express');
const router = express.Router();
const uploadImage = require('../middleware/middlewareImage');
const {
   getProductImage,
   storeProductImage,
   getProductImageById,
   updateProductImage,
   deleteProductImage
} = require('../controllers/productImageController');

router.post('/', uploadImage.single('image'), storeProductImage);
router.get('/', getProductImage);
router.get('/:id', getProductImageById);
router.put('/:id', uploadImage.single('image'), updateProductImage);
router.delete('/:id', deleteProductImage)

module.exports = router;