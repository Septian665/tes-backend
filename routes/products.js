const express = require('express');
const router = express.Router();
const {
   getProduct,
   storeProduct,
   getProductById,
   updateProduct,
   deleteProduct
} = require('../controllers/productController');

router.route('/').get(getProduct).post(storeProduct);
router.route('/:id').get(getProductById).put(updateProduct).delete(deleteProduct);
module.exports = router;