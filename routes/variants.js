const express = require('express');
const router = express.Router();
const {
   getDataVariant, 
   storeVariant, 
   getVariantById, 
   updateVariant, 
   deleteVariant 
} = require('../controllers/variantController');

router.route('/').get(getDataVariant).post(storeVariant);
router.route('/:id').get(getVariantById).put(updateVariant).delete(deleteVariant);

module.exports = router;